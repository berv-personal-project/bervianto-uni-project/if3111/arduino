#include <LiquidCrystal_I2C.h>
#define I2C_ADDR 0x3F
#define pinSensor 0
#define pinLED 5
#define pin7segmentClock 13
#define pin7segmentData 12
#define pinClock 13
#define pinData 12

int inPin = 2;         // the number of the input pin
int outPin = 8;       // the number of the output pin

int state = HIGH;      // the current state of the output pin
int reading;           // the current reading from the input pin
int previous = LOW;    // the previous reading from the input pin

// the follow variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long time = 0;         // the last time the output pin was toggled
long debounce = 200;   // the debounce time, increase if the output flickers

int pwmVal;
int incrementor;
LiquidCrystal_I2C lcd(I2C_ADDR, 16, 2);
 String stringWorld;
  int senseVal;

int displayDelay = 1000;
long displayTime = 0;
  
void setup()
{
  stringWorld = String("Hi Ade ");
  lcd.begin();
  pinMode(pinClock, OUTPUT);
  pinMode(inPin, INPUT);
  pinMode(outPin, OUTPUT);
  pinMode(pinData, OUTPUT);
  digitalWrite(pinClock, 1);
  digitalWrite(pinData, 0);
  pwmVal = 11;
  incrementor = 20;
  Serial.begin(9600);
}

int a = 0;
int data[8] = {0,0,0,0,0,0,0,0};
double lightLUX;
float floatperunit = 5 / 1024.0; // 5 voltage in arduino
float voltage;

void loop()
{
  reading = digitalRead(inPin);
  senseVal = analogRead(pinSensor);
  voltage = floatperunit * senseVal;
  lightLUX = 1333 * voltage;
  
  //button reading
  if ((reading == HIGH) && (previous == LOW) && (millis() - time > debounce)) {
    if (state == HIGH){      
      state = LOW;
    }else{
      state = HIGH;
      }
    time = millis();    
  }
  digitalWrite(outPin, state);

  previous = reading;

      if(pwmVal <= 20 || pwmVal > 235){
        incrementor = -1*incrementor;
      }
      pwmVal += incrementor;

  //all display here
  if (millis() % displayDelay == 0)
  {
      Serial.println("masuk");
      //LCD
      lcd.clear();
      lcd.print(stringWorld);
      lcd.setCursor(0,1);
      lcd.print(String(lightLUX) + " Lux");
      //PWM
      analogWrite(pinLED, pwmVal);
    
      //7segment
      for (int i=0; i<8; i++) {
        digitalWrite(pinClock, 0);
        digitalWrite(pinData, data[i]);
        digitalWrite(pinClock, 1);
      }
      displayTime = millis();
  }

  //if(millis() % displayDelay){
    //Serial.print("saya budi");
  //} 
  
  //biar gak overflow
}
